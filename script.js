const fs = require('fs');
const fetch = require("node-fetch");
const cheerio = require("cheerio");
const axios = require("axios");
const _ = require("lodash");
const Async = require('async');

async function fetchWebpage(url) {
    const response = await fetch(url, {
        credentials: "omit",
        headers: {
            accept: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "accept-language": "en-US,en;q=0.9",
            "cache-control": "no-cache",
            pragma: "no-cache",
            "sec-fetch-dest": "document",
            "sec-fetch-mode": "navigate",
            "sec-fetch-site": "same-origin",
            "sec-fetch-user": "?1",
            "upgrade-insecure-requests": "1",
        },
        referrer: "https://git.ir/search/?s=neural",
        referrerPolicy: "no-referrer-when-downgrade",
        body: null,
        method: "GET",
        mode: "cors",
    });

    return {
        cookie: response.headers.get('set-cookie').split(';')[0],
        html: await response.text(),
    };
}

async function fetchDownloadLinks(url, cookie, csrfToken) {
    const response = await fetch(url, {
        credentials: "include",
        headers: {
            "Accept": "text/html, */*; q=0.01",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.9",
            "Cache-Control": "no-cache",
            "Connection": "keep-alive",
            "Content-Length": "106",
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Cookie": cookie,
            "Host": "git.ir",
            "Origin": "https://git.ir",
            "Pragma": "no-cache",
            "Referer": url,
            "Sec-Fetch-Dest": "empty",
            "Sec-Fetch-Mode": "cors",
            "Sec-Fetch-Site": "same-origin",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.163 Safari/537.36",
            "X-Requested-With": "XMLHttpRequest",
        },
        // Referrer: url,
        // "referrerPolicy": "no-referrer-when-downgrade",
        body: `csrfmiddlewaretoken=${csrfToken}&show_download_links=1`,
        method: "POST",
        // "mode": "cors",
    });

    return await response.text();
}

async function getPageLinks(url) {
    const { html, cookie } = await fetchWebpage(url);
    const $ = cheerio.load(html);
    const csrfToken = $('meta[name="csrf_token"]').attr('content');
    const courseTitle = $('div[itemprop="articleBody"]>span.en-style.en-title').text();
    // console.log({ csrfToken, cookie });
    const linksHTML = await fetchDownloadLinks(url, cookie, csrfToken);
    // console.log({ linksHTML });

    const output = {
        title: courseTitle,
        links: cheerio
            .load(linksHTML)("a")
            .map((_, element) => element.attribs)
            .toArray()
            .map((attr) => attr.href),
    };

    if (output.links.length === 0) {
        output.links = $('a.list-group-item.list-group-item-action.download-link.list-group-item-warning')
            .map((_, element) => element.attribs)
            .toArray()
            .map((attrs) => attrs.href);
    }

    return output;
}

async function getLinks(...urls) {
    return await Promise.all(urls.map((url) => getPageLinks(url)));
}

function storeAsAria2cInputFile(courseLinks) {
    const file = fs.createWriteStream('links.txt');
    for (const course of courseLinks) {
        const links = course.links.map((link) => `${link}\n  dir=${course.title}`);
        file.write('\n' + links.join('\n'));
    }
}

if (require.main === module) {
    getLinks(...process.argv.slice(2))
        .then((result) => {
            storeAsAria2cInputFile(result);
        });
}

module.exports = {
    getLinks,
    fetchWebpage,
    storeAsAria2cInputFile,
};
