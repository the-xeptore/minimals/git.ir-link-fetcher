const qs = require('querystring');
const fetch = require('node-fetch');
const _ = require("lodash");
const inquirer = require('inquirer');
const cheerio = require('cheerio');
const helper = require('./script');


function extractPostLinks(html) {
    return cheerio.load(html)('body > main > div.container > div > div > div > div.card-body > div.row > div > a')
        .map((_, element) => element.attribs)
        .toArray()
        .map((attrs) => attrs.href)
        .map((link) => 'https://git.ir' + link)
        .map(encodeURI);
}

function extractPagesCount(html) {
    return parseInt(/(?<pages>\d+) نتیجه برای/.exec(html).groups.pages);
}

async function getPage(url) {
    const response = await fetch(url, {
        "headers": {
            "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "accept-language": "en-US,en;q=0.9",
            "cache-control": "no-cache",
            "pragma": "no-cache",
            "sec-fetch-dest": "document",
            "sec-fetch-mode": "navigate",
            "sec-fetch-site": "same-origin",
            "sec-fetch-user": "?1",
            "upgrade-insecure-requests": "1",
        },
        "referrer": "https://git.ir/",
        "referrerPolicy": "no-referrer-when-downgrade",
        "body": null,
        "method": "GET",
        "mode": "cors"
    });

    return await response.text();
}

async function search(term) {
    const body = qs.stringify({ s: term, page: 1 });
    const page = await getPage(`https://git.ir/search/?${body}`);
    const pagesCount = extractPagesCount(page);

    const allResultPages = await Promise.all(
        _.range(1, parseInt(pagesCount / 20) + 2).map((i) => {
            const body = qs.stringify({ s: term, page: i });
            return getPage(`https://git.ir/search/?${body}`);
        })
    );

    return await parseResponse(allResultPages.join('\n'));
}

async function fetchCourseTitle(url) {
    const { html } = await helper.fetchWebpage(url);
    const $ = cheerio.load(html);
    return $('div[itemprop="articleBody"]>span.en-style.en-title').text();
}

function parseResponse(response) {
    return Promise.all(
        extractPostLinks(response)
            .map((url) => fetchCourseTitle(url)
                .then((title) => ({
                    title,
                    url,
                }))
            )
    );
}

async function searchTerms(terms) {
    const results = await Promise.all(_.uniq(terms.map(search)));
    return _.uniqBy(
        _.flatten(
            results.map((result) =>
                result.map((item) => ({
                    url: item.url,
                    title: item.title,
                }))
            )
        ),
        "url"
    );
}

function searchAndWriteResluts(terms) {
    searchTerms(terms)
        .then(filterResults)
        .then(links => helper.getLinks(...links))
        .then(helper.storeAsAria2cInputFile)
}

function filterResults(results) {
    return inquirer
        .prompt([
            {
                type: 'confirm',
                name: 'defaultSelected',
                message: 'Default to be selected?',
                default: false,
            },
        ])
        .then((answers) => {
            return inquirer
                .prompt([
                    {
                        type: 'checkbox',
                        name: 'urls',
                        pageSize: 30,
                        choices: results.map((item, i) => ({
                            name: `${i + 1}) ${item.title}`,
                            value: item.url,
                            checked: answers.defaultSelected,
                        })),
                    },
                ]);
        })
        .then(answers => answers.urls);
}

if (require.main === module) {
    searchAndWriteResluts(process.argv.slice(2));
}
