# git.ir download links fetcher

A CLI app that fetches post webpages from https://git.it, then extract its download links, stores them in a file (`links.txt`) in `aria2c` input file format, grouped by their post title as `aria2c` download destination directory.
